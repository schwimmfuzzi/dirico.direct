(function () {
  'use strict';
  angular
    .module('app')
    .constant('options', {
      api: {
        baseUrl: 'http://c3-dev-cloudservice.cloudapp.net/api'
      },
      auth: {
        loginUrl: "http://c3-dev-identity.azurewebsites.net/core",
      }
    });
}());
