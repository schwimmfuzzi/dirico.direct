(function () {
  'use strict';

  angular
    .module('app')
    .service('ApiHandler', ApiHandler);

  function ApiHandler(localStorageService, $rootScope, options) {
    var self = this;

    // initialize the handler
    self.init = function () {
      self.status = {};
      self.primaryApi = localStorageService.get('primaryApi') || '';

    };

    // public setter for second api url
    self.setPrimaryApi = function (primary) {
      self.primaryApi = primary;
      localStorageService.set('primaryApi', primary);
    };

    // getter for second api url
    self.getPrimaryApi = function () {
      return self.primaryApi;
    };

    // setter for the status, after it is requested externally
    self.setStatus = function (status) {
      self.status = status;
    };

    // central getter for all factories for apiUrl
    self.getUrl = function () {
      return self.primaryApi || options.api.baseUrl;
    };

    self.get = function () {
      return 'ApiHandler';
    };

  }
}());
