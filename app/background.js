// Using chrome.identity
var manifest = chrome.runtime.getManifest();

var clientId = encodeURIComponent(manifest.oauth2.client_id);
var scopes = encodeURIComponent(manifest.oauth2.scopes.join(' '));
var redirectUri = encodeURIComponent('https://' + chrome.runtime.id + '.chromiumapp.org');

var token;

var url = "http://c3-dev-identity.azurewebsites.net/core/connect/authorize" +
          '?client_id=' + clientId +
          '&response_type=token' +
          '&redirect_uri=' + redirectUri +
          '&scope=' + scopes;

function doLogin(){
  chrome.identity.launchWebAuthFlow(
      {
          'url': url,
          'interactive':true
      },
      function(redirectedTo) {
          console.log(redirectedTo);
          if (chrome.runtime.lastError) {
              console.log(chrome.runtime.lastError.message);
          }
          else {
              setToken(redirectedTo.split('#', 2)[1]);
          }
      }
  );
}

function setToken(t) {
  token = t;
  localStorage.token = t;
}

function cleanup () {
  localStorage.removeItem('token');
}

function getToken() {
  if(token) return token;
  return false;
}
