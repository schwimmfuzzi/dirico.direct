(function() {
    'use strict';
    angular
        .module('app')
        .factory('Channel', Channel);

    function Channel($http, ApiHandler) {
        return {
            getChannelNames: function() {
                return $http.get(ApiHandler.getUrl() + '/channels/titles');
            }
        };
    }
}());
