
  'use strict';
  angular
    .module('app')
    .factory('TokenInterceptor', TokenInterceptor);

  function TokenInterceptor($q, $location, Authentication) {
    /* eslint camelcase:0 */
    return {
      request: function (config) {
        config.headers = config.headers || {};

        if (Authentication.getToken())
          config.headers.Authorization = 'Bearer ' + Authentication.getToken();

        return config;
      },

      requestError: function (rejection) {
        return $q.reject(rejection);
      },

      /* Set Authentication.isAuthenticated to true if 200 received */
      response: function (response) {
        if (response !== null && response.status === 200 && Authentication.getToken() && !Authentication.isAuthenticated)
          Authentication.isAuthenticated = true;

        return response || $q.when(response);
      },

      /* Revoke client authentication if 401 is received */
      responseError: function (rejection) {
        if (rejection !== null && rejection.status === 401 && ( Authentication.getToken() || Authentication.isAuthenticated)) {
          Authentication.unset();
          Authentication.isAuthenticated = false;
          $location.path('/login');
        }

        return $q.reject(rejection);
      }
    };
  }
