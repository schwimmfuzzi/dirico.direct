(function() {
    'use strict';
    angular
        .module('app')
        .factory('Idea', Idea);

    function Idea($http, ApiHandler) {
        return {
            create: function(idea) {
                return $http.post(ApiHandler.getUrl() + '/ideas/', idea);
            },
            getTypes: function() {
                return $http.get(ApiHandler.getUrl() + '/ideas/types');
            }
        };
    }
}());
