(function () {
  'use strict';

  angular
    .module('app')
    .factory('Authentication', Authentication);

  function Authentication(localStorageService) {
    var auth = {
      isAuthenticated: false,
      isAdmin: false,
      login: localStorageService.get('auth') || {
        token: '',
        secret: '',
        ip: ''
      }
    };

    auth.setToken = function (token) {
      auth.login.token = token;
      auth.persist();
    };

    auth.getToken = function () {
      return auth.login.token;
    };

    auth.persist = function () {
      localStorageService.set('auth', auth.login);
    };

    auth.unset = function () {
      auth.login = {};
      localStorageService.remove('auth');
    };

    return auth;
  }
}());
