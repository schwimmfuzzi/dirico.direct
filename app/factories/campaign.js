(function() {
    'use strict';
    angular
        .module('app')
        .factory('Campaign', Campaign);

    function Campaign($http, ApiHandler) {
        return {
            getCampaignTitles: function() {
                return $http.get(ApiHandler.getUrl() + '/campaigns/titles');
            }
        };
    }
}());
