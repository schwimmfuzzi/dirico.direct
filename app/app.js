'use strict';

angular.module("app", ['ui.router', 'ngMaterial', 'LocalStorageModule'])
    .config(function($httpProvider, $urlRouterProvider, $stateProvider, options) {
        $httpProvider.interceptors.push('TokenInterceptor');
        // For any unmatched url, redirect to /state1
        $urlRouterProvider.otherwise("/login");

        // Now set up the states
        $stateProvider
            .state('login', {
                url: "/login",
                templateUrl: "app/views/login.html",
                controller: 'loginCtrl'
            })

        .state('inner', {
                url: '/inner',
                templateUrl: 'app/views/inner.html',
                controller: 'innerCtrl',
                resolve: {
                    authenticated: ['$q', 'Authentication', '$state', function($q, Authentication, $state) {
                        var deferred = $q.defer();
                        if (Authentication.getToken() === '') {
                            deferred.reject('Not logged in');
                        } else
                            deferred.resolve();

                        return deferred.promise;
                    }]
                }
            })
            .state('inner.confirm', {
                url: "/confirm/:type/:status",
                templateUrl: "app/views/confirm.html",
                controller: 'confirmCtrl'
            })
            .state('inner.home', {
                url: "/home",
                templateUrl: "app/views/base.html",
                controller: 'baseCtrl'
            })
            .state('inner.home.ideas', {
                url: "/ideas",
                templateUrl: "app/views/tabs/ideas.html",
                controller: 'ideasCtrl'
            })
            .state('inner.home.pinboard', {
                url: "/pinboard",
                templateUrl: "app/views/tabs/pinboard.html",
            })
            .state('inner.home.content', {
                url: "/content",
                templateUrl: "app/views/tabs/content.html",
            });

    })
    .run(function() {

        if (chrome.extension)
            chrome.extension.getBackgroundPage().console.log('running as extension');
        else
            console.log('not running as extension');

    });
