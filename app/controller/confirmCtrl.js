'use strict';

angular.module('app')
    .controller('confirmCtrl', ConfirmCtrl);

function ConfirmCtrl($scope, $state, $stateParams) {

	$scope.init = function () {
		$scope.type = $stateParams.type;
		$scope.status = $stateParams.status;
	};

	$scope.home = function () {
		$state.go('inner.home.ideas');
	}

	$scope.init();

}
