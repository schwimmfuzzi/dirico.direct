'use strict';

angular.module('app')
    .controller('innerCtrl', InnerCtrl);

function InnerCtrl($scope, $state, Authentication) {

	$scope.logout = function () {
    Authentication.unset();
		$state.go('login');
	}

}
