'use strict';

angular.module('app')
    .controller('ideasCtrl', IdeasCtrl);

function IdeasCtrl($scope, Idea, Channel, Campaign, $state, $log, $window) {

    $scope.init = function() {
        $scope.preparedIdea = {};
        $scope.requestPending = false;
        $scope.extracted = {
            title: false,
            url: false
        };
        $scope.notification = {
            show: false,
            type: 'idea',
            status: ''
        };


        $scope.getTypes();
        $scope.getChannels();
        $scope.getCampaigns();

        $scope.extractCurrentTabInfo();
    };


    $scope.createIdea = function() {
        $scope.requestPending = true;
        Idea.create($scope.preparedIdea)
            .success(function(data) {
                $scope.notify('idea', 'success');
            })
            .error(function(err) {
                $log.error(err);
                $scope.notify('idea', 'error');
            });
    };

    $scope.getTypes = function() {
        Idea.getTypes()
            .success(function(data) {
                $scope.ideaTypes = data;
            })
            .error(function(err) {
                $log.error(err);
            });
    };

    $scope.getChannels = function() {
        Channel.getChannelNames()
            .success(function(data) {
                $scope.channels = data;
            })
            .error(function(err) {
                $log.error(err);
            });
    };

    $scope.getCampaigns = function() {
        Campaign.getCampaignTitles()
            .success(function(data) {
                chrome.extension.getBackgroundPage().console.log(data);
                $scope.campaigns = data;
            })
            .error(function(err) {
                $log.error(err);
            });
    };

    // getting information of current active browser tab (only meta, not content)
    $scope.extractCurrentTabInfo = function() {
        if (chrome.tabs) {
            chrome.tabs.query({
                active: true,
                currentWindow: true
            }, function(tab) {
                var t = tab[0];
                if (t.title) {
                    $scope.preparedIdea.title = t.title;
                    $scope.extracted.title = true;
                }

                if (t.url) {
                    $scope.preparedIdea.description = t.url;
                    $scope.extracted.url = true;
                }
            });
        } else {
            $log.log('was not able to get chrome.tabs');
        }
    };

    $scope.closeExtension = function () {
      $window.close();
    };

    // looks more complicated then it could be... but i wanted to use the same
    // signature then the forward method
    $scope.notify = function(type, status) {
        $scope.requestPending = false;
        $scope.notification.show = true;
        $scope.notification.status = status;
    };

    $scope.init();

}
