'use strict';

angular.module('app')
    .controller('loginCtrl', LoginCtrl);

function LoginCtrl($scope, Authentication, $state, localStorageService, $interval) {

    var stop, extracted;

    $scope.init = function() {
        if (Authentication.getToken())
            $scope.forward();
    }

    $scope.fakeLogin = function() {
        chrome.extension.getBackgroundPage().doLogin();
        stop = $interval(function() {
          var t = chrome.extension.getBackgroundPage().getToken();
          if(t) {
            chrome.extension.getBackgroundPage().cleanup();
            $scope.processToken(t);
          }
        }, 300);
    };

    $scope.processToken = function (t) {
      $scope.stopInterval();
      extracted = new URLSearchParams(t).get('access_token');
      $scope.setAuthAndForward(extracted);
    };

    $scope.setAuthAndForward = function (t) {
      Authentication.setToken(t);
      $scope.forward();
    };

    $scope.stopInterval = function() {
        if (angular.isDefined(stop)) {
            $interval.cancel(stop);
            stop = undefined;
        }
    };

    $scope.forward = function() {
        if(chrome.extension) chrome.extension.getBackgroundPage().cleanup();
        $state.go('inner.home.ideas');
    };

    $scope.init();
}
